from flask import Flask, render_template
from LoginForm import LoginForm
from flask_bootstrap import Bootstrap


def create_app():
    tmp = Flask(__name__)
    Bootstrap(tmp)

    return tmp


app = create_app()
app.secret_key = "yolo"


@app.route("/")
def home():
    return render_template('index.html')


@app.route("/login", methods=['GET', 'POST'])
def login():
    login_form = LoginForm()
    is_valid = login_form.validate_on_submit()

    if is_valid:
        if login_form.email.data == 'admin@email.com' and login_form.password.data == '123456789':
            return render_template('success.html')
        else:
            return render_template('denied.html')
    else:
        return render_template('login.html', form=login_form)


if __name__ == '__main__':
    app.run(debug=True)
