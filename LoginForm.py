from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, Length


class LoginForm(FlaskForm):
    email = StringField(
        label='Email',
        validators=[DataRequired(), Email()],
        description="login name"
    )
    password = PasswordField(
        label='Password',
        validators=[DataRequired(), Length(min=6, max=120)],
        description="login pw"
    )
    submit = SubmitField(
        label='Log in'
    )
